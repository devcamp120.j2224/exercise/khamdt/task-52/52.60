package com.devcamp.j5260;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Order2 {
    public int id; // id của order
    public String customerName; // tên khách hàng
    public Integer price;// tổng giá tiền
    public Date orderDate; // ngày thực hiện order
    public Boolean confirm; // đã xác nhận hay chưa?
    public String[] items; // danh sách mặt hàng đã mua
    public Person buyer;// người mua, là một object thuộc class Person

    // Khởi tạo 0 tham số
    public Order2() {
        this.id = 1108;
        this.customerName = "KhamDT";
        this.price = 250000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"Duck", "Chicken"};
        this.buyer = new Person("Dang Thanh Kham", 27, 80);
    }

    // Khởi tạo 3 tham số
    public Order2(int id, String customerName, Integer price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate =  new Date();
        this.confirm = true;
        this.items =  new String[] {"Pizza", "Hot Dog"};
        this.buyer = new Person("Ho Boi Boi", 23, 55, new String[] {"Cat", "Dog"});
    }

    // Khởi tạo 5 tham số
    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm) {
        this(id, customerName, price);
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items =  new String[] {"Burger", "Shake Potato"};
        this.buyer = new Person("Nguyen Huynh Minh Thu", 16, 48, new String[] {"Cat", "Rabbit"});
    }

    // Khởi tạo 7 tham số
    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items, Person buyer) {
        this(id, customerName, price, orderDate, confirm);
        this.items =  items;
        this.buyer = buyer;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        // Định dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi", "VN"));
        // Định dạng cho ngày tháng
        String pattern = "dd-MM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return string
        return 
            "Order [id=" + id
            + ", customerName=" + customerName
            + ", price=" + usNumberFormat.format(price)
            + ", orderDate=" + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", confirm=" + confirm
            + ", items=" + Arrays.toString(items)
            + ", buyer=" + buyer;
    }
}
